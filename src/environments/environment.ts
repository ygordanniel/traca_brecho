// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD2EtLClKCNVtu3JiSLcSq8KD4sL9dT4Go',
    authDomain: 'brechos-aa784.firebaseapp.com',
    databaseURL: 'https://brechos-aa784.firebaseio.com',
    projectId: 'brechos-aa784',
    storageBucket: 'brechos-aa784.appspot.com',
    messagingSenderId: '126857864675',
    appId: '1:126857864675:web:3fc46e11bf8b3670'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
