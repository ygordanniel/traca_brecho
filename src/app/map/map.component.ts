import { Component, OnInit, OnDestroy } from '@angular/core';

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Platform, ModalController } from '@ionic/angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapOptions,
  GoogleMapsEvent,
  Marker,
  ILatLng,
  Environment
} from '@ionic-native/google-maps';
import { Subscription } from 'rxjs';
import { Brecho } from '../models/brecho';
import { BrechoService } from '../services/firebase/firestore/brecho.service';
import { BrechoPage } from '../brecho/brecho.page';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy {
  map: GoogleMap;
  subs: Array<Subscription> = [];
  latestKownLocation: ILatLng = null;
  brechos: Map<string, Brecho> = new Map();
  isLoading = true;
  constructor(
    private platform: Platform,
    private geolocation: Geolocation,
    private brechoSvc: BrechoService,
    private modalCtrl: ModalController
  ) {}

  async ngOnInit() {
    Environment.setEnv({
      API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyD6_xDQ-LxMa7Te_ubHqcp_rHyK3gaHzEo',
      API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyD6_xDQ-LxMa7Te_ubHqcp_rHyK3gaHzEo'
    });
    await this.platform.ready();
    await this.loadMap();
    this.bindMapActions();
    this.watchUserPosition();
    this.fetchBrechos();
  }

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  async loadMap() {
    const target = {
      lat: -15.8146709,
      lng: -47.9457049
    };

    const mapOptions: GoogleMapOptions = {
      camera: {
        target,
        zoom: 13
      },
      controls: {
        myLocationButton: false,
        myLocation: true
      }
    };

    this.map = GoogleMaps.create('map_canvas', mapOptions);
  }

  private watchUserPosition() {
    const sub = this.geolocation.watchPosition().subscribe(data => {
      this.latestKownLocation = {
        lat: data.coords.latitude,
        lng: data.coords.longitude
      };
    });
    this.subs.push(sub);
  }

  centerCameraToMe() {
    this.map.animateCamera({
      duration: 2000,
      target: this.latestKownLocation
    });
  }

  private bindMapActions() {
    const mapClickSub = this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(async params => {
      const location = params[0] as ILatLng;
      const modal = await this.modalCtrl.create({
        component: BrechoPage,
        componentProps: { location }
      });
      modal.present();
    });
    this.subs.push(mapClickSub);
  }

  private fetchBrechos() {
    const sub = this.brechoSvc
      .findBrechos()
      .snapshotChanges()
      .subscribe(brechos => {
        this.isLoading = false;
        brechos.forEach(dca => {
          if (!this.brechos.get(dca.payload.doc.id)) {
            this.brechos.set(dca.payload.doc.id, dca.payload.doc.data());
            this.addMarker(dca.payload.doc.data() as Brecho);
          }
        });
      });
    this.subs.push(sub);
  }

  private addMarker(brecho: Brecho) {
    const marker = this.map.addMarkerSync({
      icon: 'blue',
      animation: 'DROP',
      position: brecho.location
    });
    marker.set('brecho', brecho);
    const sub = marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(async params => {
      const eventMarker: Marker = params[1] as Marker;
      const markerBrecho = eventMarker.get('brecho') as Brecho;
      const modal = await this.modalCtrl.create({
        component: BrechoPage,
        componentProps: { brecho: markerBrecho }
      });
      modal.present();
    });
    this.subs.push(sub);
  }
}
