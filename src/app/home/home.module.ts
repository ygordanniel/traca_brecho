import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { MapComponent } from '../map/map.component';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { BrechoPage } from '../brecho/brecho.page';
import { ContactTypePipe } from '../contact-type.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule.forRoot(),
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage, MapComponent, BrechoPage, ContactTypePipe],
  providers: [Geolocation],
  entryComponents: [BrechoPage]
})
export class HomePageModule {}
