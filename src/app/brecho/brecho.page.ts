import { Component, OnInit, Input } from '@angular/core';
import { Brecho } from '../models/brecho';
import { ModalController, ToastController } from '@ionic/angular';
import { Contact } from '../models/contact';
import { BrechoService } from '../services/firebase/firestore/brecho.service';
import { Location } from '../models/location';

@Component({
  selector: 'app-brecho',
  templateUrl: './brecho.page.html',
  styleUrls: ['./brecho.page.scss']
})
export class BrechoPage implements OnInit {
  @Input() brecho: Brecho;
  @Input() location: Location;

  contacts = [new Contact()];
  contactTypes = ['email', 'phonenumber'];
  avegareCosts = [1, 2, 3, 4, 5];
  avgCostHoveringIndex = 0;
  isLoading = false;

  constructor(
    private modalCtrl: ModalController,
    private toastCtrl: ToastController,
    private brechoSvc: BrechoService
  ) {}

  ngOnInit() {
    if (this.location) {
      this.brecho = new Brecho();
    }
  }

  removeContact(index: number) {
    this.contacts.splice(index, 1);
  }

  addNewContact() {
    this.contacts.push(new Contact());
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  onHoverAvgCost(index: number) {
    this.avgCostHoveringIndex = index;
  }

  setAvgCost(avgCost: 0 | 1 | 2 | 3 | 4) {
    this.brecho.averageCost = avgCost;
    this.avgCostHoveringIndex = avgCost;
  }

  async saveBrecho() {
    let toast;
    this.brecho.contacts = this.contacts;
    this.brecho.location = { ...this.location };
    if (this.isMinimumFieldsFilled() && !this.isLoading) {
      this.isLoading = true;
      await this.brechoSvc.saveBrecho(this.brecho).catch(async err => {
        toast = await this.toastCtrl.create({
          message: 'Ops! Algo deu errado. Tente novamente em alguns instantes.',
          duration: 5000
        });
        toast.present();
      });
      toast = await this.toastCtrl.create({
        message: 'Brecho salvo com sucesso!',
        duration: 3000
      });
      this.isLoading = false;
      toast.present();
      this.dismiss();
    } else {
      toast = await this.toastCtrl.create({
        message: 'Preencha pelo menos o nome e um contato.',
        duration: 3000
      });
      toast.present();
    }
  }

  setContact(value: string, attr: string, index: number) {
    this.contacts[index][attr] = value;
  }

  isMinimumFieldsFilled() {
    console.log('brecho ->', this.brecho);
    return this.brecho.name && this.brecho.contacts.length > 0;
  }
}
