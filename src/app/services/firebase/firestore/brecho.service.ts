import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  DocumentReference
} from '@angular/fire/firestore';
import { Brecho } from 'src/app/models/brecho';

@Injectable({
  providedIn: 'root'
})
export class BrechoService {
  readonly BRECHOS_COLLECTION = 'brechos';

  constructor(private db: AngularFirestore) {}

  findBrechos(): AngularFirestoreCollection<Brecho> {
    return this.db.collection<Brecho>(this.BRECHOS_COLLECTION);
  }

  saveBrecho(brecho: Brecho): Promise<DocumentReference> {
    return this.db.collection<Brecho>(this.BRECHOS_COLLECTION).add(brecho);
  }
}
