import { Contact } from './contact';
import { Location } from './location';

export class Brecho extends Object {
  name: string;
  instagram: string;
  site: string;
  contacts: Array<Contact> = [];
  location: Location;
  averageCost: 0 | 1 | 2 | 3 | 4;
}
