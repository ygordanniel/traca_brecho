export class Contact extends Object {
  value: string;
  type: 'email' | 'phonenumber';
}
