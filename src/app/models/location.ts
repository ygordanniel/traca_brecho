import { ILatLng } from '@ionic-native/google-maps';

export class Location extends Object {
  lat: number;
  lng: number;
}
