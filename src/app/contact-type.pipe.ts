import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contactType'
})
export class ContactTypePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    switch (value) {
      case 'email':
        return 'Email';
      case 'phonenumber':
        return 'Telefone';
      default:
        return null;
    }
  }
}
